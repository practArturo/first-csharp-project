﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Security.Cryptography;
using System.Text;
using System.Threading;

namespace WebApplication2
{
    public partial class _Default : Page
    {
        

        SqlConnection con = new SqlConnection();
        SqlCommand cmd = new SqlCommand();
        SqlDataAdapter sda = new SqlDataAdapter();
        DataTable dt = new DataTable();
        DataSet ds = new DataSet();



        protected void Page_Load(object sender, EventArgs e)
        {


            if (Session["StrNombreUsuario"] == "Ok")
            {

                con.ConnectionString = "Data Source=localhost;Initial Catalog=shop;Integrated Security=True";
                con.Open();
                if (!Page.IsPostBack)
                {
                    DataShow();
                }
            }
            else
            {
                Response.Redirect("WebForm1.aspx");
            }

          

        }

        protected void btnAnadir_Click(object sender, EventArgs e)
        {
            dt = new DataTable();
            var contrasenaEncriptada = GetMD5(txtPass.Text.ToString());
            cmd.CommandText = "Insert into usuarios (nombre,usuario,pass) values ('" + txtName.Text.ToString() + "','" + txtUsuario.Text.ToString() + "','" + contrasenaEncriptada + "')";
            cmd.Connection = con;
            Thread.Sleep(1000);
            cmd.ExecuteNonQuery();
            DataShow();
            UpdatePanel1.Update();

          
        }

        public void DataShow()
        {
            ds = new DataSet();
            cmd.CommandText = "Select * from usuarios";
            cmd.Connection = con;
            sda = new SqlDataAdapter(cmd);
            sda.Fill(ds);
            cmd.ExecuteNonQuery();
            GridView1.DataSource = ds;
            GridView1.DataBind();
            con.Close();


        }


        protected void btnEditar_Click(object sender, EventArgs e)
        {
            dt = new DataTable();
            var contrasenaEncriptada = GetMD5(txtPass.Text.ToString());
            cmd.CommandText = "Update usuarios set usuario='" + txtUsuario.Text.ToString() +"', pass='"+ contrasenaEncriptada + "' where nombre='" + txtName.Text.ToString() + "'";
            cmd.Connection = con;
            cmd.ExecuteNonQuery();
            DataShow();
            UpdatePanel1.Update();
        }

        protected void btnEliminar_Click(object sender, EventArgs e)
        {
            dt = new DataTable();
            cmd.CommandText = "Delete from usuarios where nombre='" + txtName.Text.ToString() + "'";
            cmd.Connection = con;
            cmd.ExecuteNonQuery();
            DataShow();
            UpdatePanel1.Update();
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            lblModalTitle.Text = "Introduzca un usuario";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "$('#myModal').modal();", true);
            upModal.Update();
        }

        public static string GetMD5(string str)
        {
            MD5 md5 = MD5CryptoServiceProvider.Create();
            ASCIIEncoding encoding = new ASCIIEncoding();
            byte[] stream = null;
            StringBuilder sb = new StringBuilder();
            stream = md5.ComputeHash(encoding.GetBytes(str));
            for (int i = 0; i < stream.Length; i++) sb.AppendFormat("{0:x2}", stream[i]);
            return sb.ToString();
        }

    }
}