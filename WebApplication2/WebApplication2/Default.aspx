﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="WebApplication2._Default" %>


<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">


    <div class="container">
    <div class="btn-group" style="display: flex;  float:right; margin-top: 10px;">
        <asp:Button ID="btnSubmit" CssClass="btn btn-primary " runat="server" Text="Añadir"          
        OnClick="btnSubmit_Click"></asp:Button>
    </div>
</div>



<!-- Bootstrap Modal Dialog -->
<div class="modal fade" id="myModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <asp:UpdatePanel ID="upModal" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
            <ContentTemplate>
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title"><asp:Label ID="lblModalTitle" runat="server" Text=""></asp:Label></h4>
                    </div>
                    <div class="modal-body">
                        
                        Nombre: <asp:TextBox ID="txtName" runat="server" style="max-width: 100%" CssClass="form-control" ></asp:TextBox>
                       
                        <br /> 
                        Usuario: <asp:TextBox ID="txtUsuario" runat="server" style="max-width: 100%"  CssClass="form-control"  ></asp:TextBox>
                        <br />
                        Pass: <asp:TextBox ID="txtPass" runat="server"  style="max-width: 100%" CssClass="form-control" ></asp:TextBox>
                        <br />
                        <div class="text-center">
                            <asp:Button runat="server" id="btnAnadir" CssClass="btn btn-success " OnClick="btnAnadir_Click"  Text="Añadir" OnClientClick="ToggleValidator()" />
                            <asp:Button runat="server" ID="Button1" CssClass="btn btn-warning " OnClick="btnEditar_Click" Text="Editar" />
                            <asp:Button runat="server" ID="Button2"  CssClass="btn btn-danger " OnClick="btnEliminar_Click" Text="Eliminar" />
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-info" data-dismiss="modal" aria-hidden="true">Close</button>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</div>


    <div style="margin-top: 10px;">

                <asp:UpdatePanel ID="UpdatePanel1" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
            <ContentTemplate>
        <asp:GridView ID="GridView1" runat="server" 
            CssClass="table table-hover table-striped" GridLines="None" 
            AutoGenerateColumns="False">
            <Columns>
                <asp:BoundField DataField="id" HeaderText="id" Visible="false" />
                <asp:BoundField DataField="nombre" HeaderText="Nombre" />
                <asp:BoundField DataField="usuario" HeaderText="Usuario" />
                <asp:BoundField DataField="pass" HeaderText="Pass" />
            </Columns>
            <RowStyle CssClass="cursor-pointer" />
         </asp:GridView>
           </ContentTemplate>
        </asp:UpdatePanel>

        </div>


   




</asp:Content>
