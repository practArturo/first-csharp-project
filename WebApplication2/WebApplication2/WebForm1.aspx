﻿<%@ Page Title="About" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="WebApplication2.WebForm1" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
<div class="container">
    <div style="display: flex; justify-content: center; align-items: center; height: 80vh" >
        <div class="center-block">
            <h2 class="form-signin-heading">
                Login</h2>
            <label for="txtUsername">
                Username</label>
            <asp:TextBox ID="txtUsername" runat="server" CssClass="form-control" placeholder="Enter Username"
                required="" />
            <br />
            <label for="txtPassword">
                Password</label>
            <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" CssClass="form-control"
                placeholder="Enter Password" required="" />
            <br />
            <asp:Button ID="btnLogin" Text="Login" runat="server"  OnClick="validator" Class="btn btn-primary" />
            <br />
            <br />
            <div id="dvMessage" runat="server" visible="false" class="alert alert-danger">
                <strong>Error!</strong>
                <asp:Label ID="lblMessage" runat="server" />
            </div>
        </div>
   
    </div>
</div>
</asp:Content>
